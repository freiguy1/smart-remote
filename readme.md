# ATmega48 Smart Remote

This is a project born out of laziness. But as I think about it, perhaps most are. In my bedroom one remote for controlling our TV is not enough. It mainly resides on my side of the bed. However if I fall asleep before my lovely wife while the TV is still on, her only real option is to get out of the warm bed, walk to the TV and physically turn the TV off when she wants to sleep. A true tragedy.

A secondary motivation for this project is that it allows me to use my new oscilloscope! It simply wouldn't be possible without some high frequency voltage monitor.


## The plan

The smart remote will be programmable based on signals sent by your primary remote. In other words, you use your existing remote to program each button on the smart remote, then it acts like an auxiliary remote.

## Hardware

This device will contain 5 main components:

1. Microcontroller: the brain. This contains the code for making the thing work. I'm choosing an ATmega48. It's cheap at around $2, easy to solder, super low power, and has EEPROM for storing the code for each button. This 8-bit tech is a bit old compared to the increasingly popular 32-bit ARM alternatives, but I own a few of these and honestly I enjoy coding for these minimal microcontrollers. It's fun to see what such a little chip is capable of. If you're unfamiliar with it, this chip is in the same family as chip used on the extremely popular [Arduino Uno](https://store.arduino.cc/usa/arduino-uno-rev3). It just has less flash, RAM and EEPROM.
2. Buttons: one "record" button and 5-15 "emitter" buttons.
3. Infrared detector: this is responsible for listening to your primary remote when recording signals
4. Infrared emitter: this sends the code through the air to your TV.
5. Batteries: I think two AAAs or AAs will work for a long time.


## ATmega48 Peripherals

I thought it would be useful to record how I'm using some of the peripherals on the microcontroller. If not for your benefit, then for mine.

### Timer/Counter 0 (8-bit)

My TV uses the [NEC Infrared Transmission Protocol](https://techdocs.altium.com/display/FPGA/NEC+Infrared+Transmission+Protocol), so that is what this project uses. This protocol requires a 38kHz carrier signal. That means the infrared LED is never on, solidly. Instead when it's sending an "on" signal, it's pulsing at 38kHz. This counter's job will be toggling a pin at 38kHz. To do that, I set up the timer in "clear timer on compare match" mode; certainly a mouthful of a mode. This mode has the capability for the timer to count up to a particular value then start back at 0. Every time it hits that particular value, it can toggle a pin.

We need to calculate that magic number. We can get pretty close with theory but since I'm using the internal oscillator, it'll probably need to be tweaked. So this counter will run at 8mHz, and we're looking to get the LED to toggle at 38kHz. 8,000,000 / 38,000 = 211. However this value would make the LED blink at half speed because iit's turning off at 211 and then back on at 211. We want a full period in 211 ticks, so instead let's do 8,000,000 / 78,000 = 105.

The minimal code to set the counter up this way:

```c
TCCR0A = (1 << COM0B0) // Toggle B on compare match
       | (1 << WGM01); // Clear timer on compare match
TCCR0B = (1 << CS00); // Use system clock (8Mhz)
OCR0A = 105; // number of ticks at 8Mhz for 38khz pulse
```

### Timer/Counter 1 (16-bit)

This counter performs two roles. It depends if we're receiving a new IR code or transmitting one. Since the actual code gets pulsed on the order of ~half milliseconds, this counter will have its clock be F_CPU / 8 which is 1MHz.

#### Receiving

While receiving, the timer is set up in input compare mode. This mode captures the current counter value when a pin changes then raises an interrupt. Inside the interrupt, you read the stored counter value and this tells us whether a 0 or 1 was sent! Here's some basic setup code for input capture mode.

```c
TCCR1A = 0x00;
// Don't set ICES1 so this gets triggered on a falling edge
TCCR1B = (1 << ICNC1)  // enable input capture noise canceler
       | (1 << CS11); // divide clock by 8
TIMSK1 = (1 << ICIE1); // Enable interrupt for input compare
TCNT1 = 0x0000; // start counter at 0
```

#### Transmitting

For transmitting, the NEC protocol sends the actual button code atop that 38kHz carrier signal. Counter 1's responsibility then will be toggling Counter 0 at the precise moment as detailed in the NEC protocol for 0s and 1s. Like Counter 0, we'll use "clear timer on compare match" mode: count up to a certain value, then restart at 0. The value we count up to will be exactly how long the 38kHz signal should be on, then off, then on, then off.

As an example, to send a `1`, the protocol states "a 562.5µs pulse burst followed by a 1.6875ms space." Well this is pretty easy to calculate at 1Mhz. Each tick will be 1µs, so 562.5µs is 562 ticks, and 1.6875ms is 1,687 ticks. Cool!
