#include <avr/io.h>       //sets up some constants and 
                          // names of pins
#include <util/delay.h>   //gives me _delay_ms() and 
                          // _delay_us()
#include <avr/interrupt.h>
#include <avr/sleep.h>


#define ZERO_BIT_DELAY 1163 // 2210
#define ONE_BIT_DELAY 2327 // 4479
#define INIT_DELAY 13964 // 26912
#define DELAY_THRESHOLD 100
#define ZERO_BIT_DELAY_LOW ZERO_BIT_DELAY - DELAY_THRESHOLD
#define ZERO_BIT_DELAY_HIGH ZERO_BIT_DELAY + DELAY_THRESHOLD
#define ONE_BIT_DELAY_LOW ONE_BIT_DELAY - DELAY_THRESHOLD
#define ONE_BIT_DELAY_HIGH ONE_BIT_DELAY + DELAY_THRESHOLD
#define INIT_DELAY_LOW INIT_DELAY - DELAY_THRESHOLD
#define INIT_DELAY_HIGH INIT_DELAY + DELAY_THRESHOLD

volatile uint8_t bitCounter = 0;
volatile uint32_t command = 0x0505A0A0;

// Recording state machine
#define R_OFF 0
#define R_WAIT_BUTTON 1
#define R_RECORDING 2

volatile uint8_t recordingState = R_OFF;
volatile uint8_t recordingButtonIndex = 0;

#define RECORDING_TIMEOUT_TICKS 5 * 9 //5 seconds. buttons checked at 9.25hz
volatile uint8_t recordingTimeoutCounter = 0;

// Transmitting state machine
#define T_OFF 0
#define T_START 1
#define T_LEADING_PULSE 2
#define T_BITS 3
#define T_FINAL 4

volatile uint8_t transmittingState = T_OFF;

// button related things
volatile uint8_t previousButtonsState = 0;
volatile uint8_t buttonPollCounter = 0;
volatile uint8_t userLedState = 0;

volatile uint32_t commands[3] = { 0x5555AAAA, 0x1717C8C8, 0x2525C0C0 };

void startEmitter() {
    TIMSK0 = 0; // disable all interrupts
    PORTD |= (1 << PORTD0); // Start ON
    TCCR0A = (1 << COM0B0) // Toggle B on compare match
             | (1 << WGM01); // Clear timer on compare match
    TCCR0B = (1 << CS00); // Use system clock (8Mhz)
    OCR0A = 100; // ticks at 8Mhz for 38khz pulse
}

void stopEmitter() {
    TCCR0B = 0; // no clock input = off
    TCCR0A = 0; // release pin D3 to be turned off
    PORTD &= ~(1 << PORTD0); // end OFF
}

void startRecording() {
    TCCR1A = 0x00;
    TCCR1B = (1 << ICNC1)  // enable input capture noise canceler
             | (1 << CS11); // divide clock by 8
    TIMSK1 = (1 << ICIE1); // Enable interrupt for input compare
    TCNT1 = 0x0000; // start counter at 0
}

void stopRecordingAndTransmit() {
    // Disable counter
    TCCR1A = 0x00;
    TCCR1B = 0x00;
}

void startTransmit() {
    bitCounter = 0;
    transmittingState = T_START;
    TIFR1 |= (1 << OCF1A); // clear interrupt flag
    TCNT1 = 0x0000; // start counter at 0
    TIMSK1 = (1 << OCIE1A); // Enable interrupt for compare match
    TCCR1A = 0x00;
    TCCR1B = (1 << WGM12)  //  clear timer on compare match mode
             | (1 << CS11); // divide clock by 8
    startEmitter();
    OCR1A = 9310; // 9 milliseconds
}

void toggleUserLed() {
    PINB |= (1 << PIN7);
    userLedState ^= 1;
}

void setUserLed(uint8_t state) {
    if (state) {
        userLedState = 1;
        PORTB |= (1 << PORTB7);
    } else {
        userLedState = 0;
        PORTB &= ~(1 << PORTB7);
    }
}

uint8_t getNewButtonsState() {
    // Inverting pins because when a pin is 0, button is pushed
    return ~PINC & 0x0F;
}

void handleButtons(uint8_t buttonsState) {
    // Handle timeout for recording
    if (recordingState == R_WAIT_BUTTON) {
        recordingTimeoutCounter++;
        if (recordingTimeoutCounter > RECORDING_TIMEOUT_TICKS) {
            setUserLed(0);
            recordingState = R_OFF;
            recordingTimeoutCounter = 0;
        }
    }


    for (uint8_t i = 0; i < 4; i++) {
        uint8_t buttonPressed = buttonsState & (1 << i);
        uint8_t buttonWasPressed = previousButtonsState & (1 << i);
        // Note: could also sense button release if wanted
        if (buttonPressed && buttonWasPressed) {
            // held down
        } else if (buttonPressed) {
            // Pressed
            if (i == 3) {
                // Record button pressed
                if (recordingState == R_WAIT_BUTTON) {
                    recordingState = R_OFF;
                    setUserLed(0);
                    recordingTimeoutCounter = 0;
                } else {
                    recordingState = R_WAIT_BUTTON;
                    setUserLed(1);
                }
            } else {
                if (recordingState == R_WAIT_BUTTON) {
                    recordingTimeoutCounter = 0;
                    recordingButtonIndex = i;
                    startRecording();
                } else {
                    command = commands[i];
                    setUserLed(1);
                    startTransmit();
                }
            }
        }
    }
}

ISR(TIMER1_CAPT_vect) {
    uint16_t timerValue = ICR1;
    TCNT1 = 0x0000; // reset counter back to 0
    //Serial.println(timerValue);
    //return;

    uint8_t isRecordingBits = recordingState == R_RECORDING;

    if (isRecordingBits) {
        if (timerValue >= ONE_BIT_DELAY_LOW && timerValue <= ONE_BIT_DELAY_HIGH) {
            command = command | ((uint32_t)1 << bitCounter);
        }
        bitCounter ++;
    } else if (!isRecordingBits && timerValue >= INIT_DELAY_LOW && timerValue <= INIT_DELAY_HIGH) {
        bitCounter = 0;
        command = 0;
        recordingState = R_RECORDING;
        setUserLed(1);
    } else {
        bitCounter = 0;
    }
}

ISR(TIMER1_COMPA_vect) {
    // First, toggle emitter
    uint8_t wasEmitting = TCCR0B;
    if (wasEmitting) {
        stopEmitter();
    } else {
        startEmitter();
    }

    // Then decide how long to wait until this interrupt gets called again
    if (transmittingState == T_START && wasEmitting) {
        OCR1A = 4655;
    } else if (transmittingState == T_START) {
        transmittingState = T_BITS;
        OCR1A = 580;
    } else if (transmittingState == T_BITS && !wasEmitting && bitCounter < 32) {
        // new bit, increment counter and stay off for short 562 us
        OCR1A = 580;
        bitCounter++;
    } else if (transmittingState == T_BITS && wasEmitting) {
        if (command & ((uint32_t)1 << bitCounter)) {
            OCR1A = 1745;
        } else {
            OCR1A = 580;
        }
    } else {
        setUserLed(0);
        stopRecordingAndTransmit();
        stopEmitter();
    }
}

ISR(TIMER2_COMPA_vect) {
    // Button polling interrupt
    // Only actually check the buttons once every 4 times this gets ran
    buttonPollCounter++;
    if (buttonPollCounter < 4) 
        return;

    buttonPollCounter = 0;
    uint8_t buttonsState = getNewButtonsState();
    handleButtons(buttonsState);
    previousButtonsState = buttonsState;
}

int main( void ){
    // Set user LED to output
    DDRB |= (1 << DDRB7);
    setUserLed(0);

    // Set buttons to input
    DDRC &= ~((1 << DDRC0)
            | (1 << DDRC1)
            | (1 << DDRC2)
            | (1 << DDRC3));
    // Set buttons to pull-up
    PORTC |= (1 << PORTC0)
            | (1 << PORTC1)
            | (1 << PORTC2)
            | (1 << PORTC3);

    // Set up Timer/Counter 2 as button poller
    OCR2A = 210; // overflow at 37hz (26.9ms) which is 4x as fast as needed for repeat code (108ms)
    TIMSK2 = (1 << OCIE2A); // Enable interrupt for compare match
    TCCR2A = (1 << WGM21); // Clear timer on compare match mode
    TCCR2B = (1 << CS22) | (1 << CS21) | (1 << CS20); // divide clock by 1024 (7,812hz)

    DDRB &= ~(1 << DDRB0); // Set IR receiver pin to input
    PORTB |= (1 << PORTB0); // Pull up IR receiver pin

    DDRD |= (1 << DDRB5); // IR emitter output (OC2A);

    PRR &= ~(1 << PRTIM1); // enable timer 1 in power register

    sei();

    while (1) {
        // set_sleep_mode(SLEEP_MODE_IDLE);
        // sleep_enable();

        //Serial.println("main loop");

        // if (!buttonIsPressed && newButtonIsPressed) {
        //     stopRecordingAndTransmit();
        //     startTransmit();
        //     // startEmitter();
        //     buttonIsPressed = newButtonIsPressed;
        // } else if (buttonIsPressed && !newButtonIsPressed) {
        //     // stopEmitter();
        //     buttonIsPressed = newButtonIsPressed;
        // }

        if (bitCounter == 32 && recordingState == R_RECORDING) {
            recordingState = R_OFF;
            stopRecordingAndTransmit();
            commands[recordingButtonIndex] = command;
            setUserLed(0);
        }
    }
}

