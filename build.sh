#!/bin/bash

build() {
    avr-gcc -mmcu=atmega48pa -DF_CPU=8000000UL -O -o bin/main.bin src/main.c && \
    avr-objcopy -O ihex bin/main.bin bin/main.hex
}

mkdir -p bin

case "$1" in
    build)
        build
        ;;
    burn)
        build && avrdude -c usbtiny -p m48p -U flash:w:bin/main.hex
        ;;

    fuses)
        # default exept do not divide clock by 8
        avrdude -c usbtiny -p m48p -U lfuse:w:0xe2:m -U hfuse:w:0xdf:m -U efuse:w:0xff:m
        ;;
    clean)
        rm -rf bin
        ;;
    *)
        echo "Usage: $0 {build|burn|fuses|clean}"
        exit 2
esac

